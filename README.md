# Svelte starter

Svelte starter is an [svelte](https://svelte.dev/) template that includes WebPack, miniCss and babel-loader.

To create a new project based on this template

```bash
git clone https://gitlab.com/cld_rojas/svelte-starter <<svelte-app>>
cd svelte-app
```

Note that you will need to have [node](https://nodejs.org/) installed.

## Get Started

Install the dependencies....

```bash
cd svelte-app
npm install
```

...then start webpack:

```bash
npm run dev
```

Navigate to localhost:8080. You should see your app running. Edit a component file in `src`, save it, and the page should reload with your changes.

## License

[ISC](https://choosealicense.com/licenses/isc/)
